# Simulator of basic model of Turing machine

Simulator of basic model of Turing machine. Implemented in Java.

My lab assignment in Introduction to Theoretical Computer Science, FER, Zagreb.

Created: 2019
