import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;


public class SimTS {
	
	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		String states = reader.readLine();
		String alphabet = reader.readLine();
		String trackAlphabet = reader.readLine();
		String emptyCellCh =  reader.readLine();
		String trackInTxt = reader.readLine();
		String acceptableStates = reader.readLine();
		String initialState = reader.readLine();
		String trackInitialIndex = reader.readLine();

		List<String> transitions = new ArrayList<>();

		String t = reader.readLine();
		while (t != null && !t.trim().isEmpty()) {
			transitions.add(t);
			t = reader.readLine();
		}

		String[] statesQ = states.split(",");
		String[] alphabetS = alphabet.split(",");
		String[] trackAlphabetS = trackAlphabet.split(",");
		String[] acceptableStatesQ = acceptableStates.split(",");
		String initialStateQ = initialState;
		int trackInitialIndexInd = Integer.parseInt(trackInitialIndex.trim());

		if (acceptableStatesQ.length == 1 && acceptableStatesQ[0].isEmpty())
			acceptableStatesQ = new String[0];

		List<String> acceptables = Arrays.asList(acceptableStatesQ);
		
		
		Map<TableRowLeft,TableRowRight> rows = makeTable(transitions, acceptables);
		
		String currState = initialStateQ;
		String[] currTrack = new String[trackInTxt.length()];
		final int currTrackLastInd = currTrack.length-1;
		char[] inTrackChs = trackInTxt.toCharArray();
		for (int i=0, n=inTrackChs.length; i<n; i++) {
			currTrack[i] = inTrackChs[i]+"";
		}
		int currInd = trackInitialIndexInd;
		String currChar = currTrack[currInd];
		TableRowLeft rowKey = new TableRowLeft(currState, currChar);
		
		while (rows.containsKey(rowKey)) {
			if ((currInd == 0 && rows.get(rowKey).shift.equals("L")) || (currInd == currTrackLastInd && rows.get(rowKey).shift.equals("R"))) {
				break;
			}
			
			TableRowRight rowValue = rows.get(rowKey);
			currState = rowValue.nextState;
			currTrack[currInd] = rowValue.outChar;
			currInd = rowValue.shift.equals("L") ? (currInd-1) : (currInd+1);
			currChar = currTrack[currInd];
			
			rowKey = new TableRowLeft(currState, currChar);
		}
		
		StringBuilder printTrack = new StringBuilder("");
		for (String s : currTrack) {
			printTrack.append(s);
		}
		String printAcceptable = acceptables.contains(currState) ? "1" : "0";
		
		System.out.println(currState+"|"+currInd+"|"+printTrack+"|"+printAcceptable);
		
		reader.close();
	}
	
	
	private static Map<TableRowLeft,TableRowRight> makeTable(List<String> transitions, List<String> acceptables) {
		Map<TableRowLeft,TableRowRight> tableRows = new LinkedHashMap<>();
		for (String t : transitions) {
			String[] lr = t.split("->");
			String[] l = lr[0].split(",");
			String currentQ = l[0];
			String currentCh = l[1];
			String[] r = lr[1].split(",");
			String newState = r[0];
			String newCh = r[1];
			String shift = r[2];

			tableRows.put(new TableRowLeft(currentQ, currentCh), new TableRowRight(newState, newCh, shift));
		}

		return tableRows;

	}

	private static class TableRowLeft {
		private String currentState;
		private String inChar;
		
		public TableRowLeft(String currentState, String inChar) {
			super();
			this.currentState = currentState;
			this.inChar = inChar;
		}

		public String getCurrentState() {
			return currentState;
		}

		public String getInChar() {
			return inChar;
		}

		@Override
		public int hashCode() {
			return Objects.hash(currentState, inChar);
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (!(obj instanceof TableRowLeft))
				return false;
			TableRowLeft other = (TableRowLeft) obj;
			return Objects.equals(currentState, other.currentState) && Objects.equals(inChar, other.inChar);
		}

		@Override
		public String toString() {
			return currentState + "," + inChar;
		}
		
		
	}
	
	private static class TableRowRight {
		private String nextState;
		private String outChar;
		private String shift;
		
		public TableRowRight(String nextState, String outChar, String shift) {
			super();
			this.nextState = nextState;
			this.outChar = outChar;
			this.shift = shift;
		}

		public String getNextState() {
			return nextState;
		}

		public String getOutChar() {
			return outChar;
		}

		public String getShift() {
			return shift;
		}

		@Override
		public int hashCode() {
			return Objects.hash(nextState, outChar, shift);
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (!(obj instanceof TableRowRight))
				return false;
			TableRowRight other = (TableRowRight) obj;
			return Objects.equals(nextState, other.nextState) && Objects.equals(outChar, other.outChar)
					&& Objects.equals(shift, other.shift);
		}

		@Override
		public String toString() {
			return  nextState + "," + outChar + "," + shift;
		}
		
		
	}

}
